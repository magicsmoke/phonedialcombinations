# Requirements
- PHP >=7.0

# Running the application
- At root path execute in the command line: `php bootstrap.php <number>`

# Todo
- Parse combinations and construct output. Right now only the combinations structure is being dump.
