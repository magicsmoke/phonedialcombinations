<?php

interface InputInterface
{
    public function getDigits(): array;
}