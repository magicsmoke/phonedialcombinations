<?php

class CombinationsGenerator
{
    public $map = [];

    public $input;

    public $structure = [];

    public function __construct($map, $input) {
        $this->map = $map;
        $this->input = $input;
    }

    public function printCombinations (): void {
        $this->buildStructure($this->structure, 0);
        print_r($this->structure); // @TODO
    }

    protected function buildStructure(&$structure, $inputPosition) {
        $lettersFromPosition = $this->map[$this->input->getDigits()[$inputPosition]];
        $nextPosition = $inputPosition + 1;

        if (isset($this->input->getDigits()[$nextPosition])) {
            foreach($lettersFromPosition as $letter) {
                $structure[$letter][] = $this->buildStructure(
                                                        $structure[$letter],
                                                        $nextPosition);
            }
        } else {
            return $lettersFromPosition;
        }
    }
}
