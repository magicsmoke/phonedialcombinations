<?php

    require_once('CombinationsGenerator.php');
    require_once('InputManager.php');

    $map = [
        '1' => [],
        '2' => ['A','B','C'],
        '3' => ['D','E','F'],
        '4' => ['G','H','I'],
        '5' => ['J','K','L'],
        '6' => ['M','N','O'],
        '7' => ['P','Q','R','S'],
        '8' => ['T','U','V'],
        '9' => ['W','X','Y','Z'],
    ];

    $userInput = isset($argv[1]) ? $argv[1] : '';

    try {
        $inputManager = (new InputManager())
            ->allowChars(array_keys($map))
            ->verify($userInput);

        (new CombinationsGenerator($map, $inputManager))->printCombinations();
        
    } catch (Exception $e) {
        echo $e->getMessage();
    }
