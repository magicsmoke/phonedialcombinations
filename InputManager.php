<?php

require_once('InputInterface.php');

class InputManager implements InputInterface
{
    private $input = [];

    private $allowedChars = [];

    public function verify(string $input): object {
        $split = $this->split($input);
        
        foreach($split as $char) {
            if (!in_array($char, $this->allowedChars)) {
                throw new Exception("Only the following digits are allowed: " . implode(',', $this->allowedChars) . "\n");
            }
        }

        $this->input = $split;

        return $this;
    }

    public function allowChars(array $chars): object {
        $this->allowedChars = $chars;

        return $this;
    }

    private function split(string $input): array {
        return str_split($input);
    }

    public function getDigits(): array {
        return $this->input;
    }
}
